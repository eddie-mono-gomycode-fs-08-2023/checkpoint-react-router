import React from 'react';
// This component will display individual movie information.
const MovieCard = ({ title, description, posterURL, rating }) => {
  return (
    <div className="movie-card">
      <img src={posterURL} alt={title} />
      <h2>{title}</h2>
      <p className='description'>{description}</p>
      <p className='rate'>Rating: {rating}</p>
    </div>
  );
};

export default MovieCard;