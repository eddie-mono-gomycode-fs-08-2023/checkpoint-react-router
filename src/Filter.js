import React from 'react';
// Setting title and rating filters.
const Filter = ({ filterTitle, setFilterTitle, filterRating, setFilterRating }) => {
  return (
    
    <div className="filter">
    <h3>MY MOVIE APP</h3>
    <div className='search-filter'>
      <input className='search-bar'
        type="text"
        placeholder="Filter by Title"
        value={filterTitle}
        onChange={(e) => setFilterTitle(e.target.value)}
      />
      <input className='search-rate-bar'
        type="number"
        placeholder="Filter by Rating"
        value={filterRating}
        onChange={(e) => setFilterRating(e.target.value)}
      />
      </div>
    </div>
  );
};

export default Filter;